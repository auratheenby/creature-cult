class_name PlayerInput
extends Node


signal possess_recieved
signal vector_changed(vector: Vector2)
signal move_action_started
signal move_action_stopped


var vector: Vector2:
	set(new):
		vector = new;
		vector_changed.emit(vector);


func _process(delta: float) -> void:
	vector = Input.get_vector("move_left", "move_right", "move_up", "move_down");


func get_vector() -> Vector2:
	return vector
