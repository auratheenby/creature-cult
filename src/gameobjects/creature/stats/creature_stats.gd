class_name CreatureStats
extends Resource

## Keeps track of the stats for a [Creature]
#TODO figure out a way for mods to add custom stats


var DEFAULTS := {
}


var _data: Dictionary


func _init() -> void:
	_data.merge(DEFAULTS)
#	_data.make_read_only()


func get_stat(stat: String) -> float:
	# base value
	var value: float = _data[stat].value
	# applying modifiers
	for modifier: StatModifier in _data[stat].modifiers:
		value = modifier.calculate(value)
	return value
	

func set_stat(stat: String, value: float) -> void:
	# clamp value between min and max, if they exist
	value = _clamp_stat(stat, value)
	_data[stat].value = value


func add_modifier(stat: String, modifier: StatModifier) -> void:
	_data[stat].modifiers.append(modifier)


func apply_instant_modifier(stat: String, modifier: StatModifier) -> float:
	var value := modifier.calculate(get_stat(stat))
	set_stat(stat, value)
	return get_stat(stat)


func get_modifiers(stat: String) -> Array[StatModifier]:
	return _data[stat].modifiers.duplicate() as Array[StatModifier]


func remove_modifier(stat: String, modifier: StatModifier) -> void:
	_data[stat].modifiers.erase(modifier)


func remove_modifier_by_index(stat: String, idx: int) -> void:
	if _data[stat].modifiers.is_empty(): return
	if idx < 0: idx += _data[stat].modifiers.size()
	_data[stat].modifiers.remove_at(idx)


func clear_modifiers(stat: String) -> void:
	_data[stat].modifiers.clear()


func _clamp_stat(stat: String, value: float) -> float:
	return clampf(value, _data[stat].min, _data[stat].max)
