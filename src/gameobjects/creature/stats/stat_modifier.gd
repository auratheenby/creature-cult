class_name StatModifier
extends Resource


## Takes an expression, then calculates a modified value on demand.

@export_multiline
## Parsed into an [Expression],
## and the execute result will be returned as the new value.
## You have access to [code]value[/code],
## and any [@GlobalScope] constants or functions.
var expression := "value":
	set(v):
		expression = v
		_parse_error = _expression.parse(expression, ["value"])


var _expression := Expression.new()
var _parse_error: int


func _init(expr := "") -> void:
	if expr: expression = expr


## Modifies the value according to the expression and returns it.
## If anything goes wrong, it returns the value unchanged,
## so make sure [member expression] is valid.
func calculate(value: float) -> float:
	if _parse_error != OK: return value
	var result: float = _expression.execute([value])
	if _expression.has_execute_failed():
		return value
	return result
