class_name Movement2D
extends Node

## In charge of moving its parent node, which must have the required interface.
##
## Has a [member direction] and [member speed], and it will automatically do its job accordingly.
## Uses the builtin virtual method [method _physics_process], so you can control its processing using
## [method _set_phyiscs_process], [member process_mode], and [member process_physics_priority]. 

signal speed_changed(new: float)
signal direction_changed(new: Vector2)

## The speed that the gameobject will move at.
@export
var speed := 300.0;
## The direction to move in.
@export
var direction := Vector2.ZERO;

@onready
var _parent: Node2D = get_parent();

var _mirror: Movement2D;
var _cached_speed := 0.0;


func _physics_process(delta: float) -> void:
	_parent.velocity = direction * speed;
	_parent.move_and_slide();


## Sets [member direction]. Mostly for connecting signals,
func set_direction(new: Vector2) -> void:
	direction = new.normalized();
	direction_changed.emit(new);


## Sets [member speed]. Mostly for connecting signals.
func set_speed(new: float) -> void:
	speed = new;
	speed_changed.emit(new)


func _on_Puppetmaster_possessed(puppet: Node) -> void:
	_mirror = puppet.get_node_or_null("../Movement2D");
	if _mirror == null: return;
	_cached_speed = speed;
	speed = _mirror.speed;
	_mirror.speed_changed.connect(set_speed);
	direction_changed.connect(_mirror.set_direction);


func _on_Puppetmaster_unpossessed(puppet: Node) -> void:
	if is_instance_valid(_mirror):
		_mirror.speed_changed.disconnect(set_speed);
		direction_changed.disconnect(_mirror.set_direction);
	_mirror = null;
	speed = _cached_speed;
