@icon("puppet.png")
class_name Puppet
extends Node

## Enables the parent node to be controlled by a [Puppetmaster].


signal possessed(master: Puppetmaster)
signal unpossessed(master: Puppetmaster)

var _master: Puppetmaster


func get_possessed(master: Puppetmaster) -> bool:
	if master.get_puppet() == self:
		_master = master
		possessed.emit(master)
		return true
	return false


func get_unpossessed(master: Puppetmaster) -> bool:
	if not master.get_puppet() == self:
		_master = null
		unpossessed.emit(master)
		return true
	return false
