@icon("puppeteer.png")
class_name Puppetmaster
extends Node

## Enables control of a [Puppet].

signal possessed(puppet: Puppet)
signal unpossessed(puppet: Puppet)


var _puppet: Puppet;


func possess(puppet: Puppet) -> void:
	if _puppet == puppet: return;
	if has_puppet():
		unpossess(_puppet)
		if has_puppet(): return;
	_puppet = puppet;
	if not puppet.get_possessed(self):
		puppet = null;
		return;
	_connect_puppet(puppet);
	possessed.emit(puppet)


func unpossess(puppet: Puppet) -> void:
	_puppet = null;
	if not puppet.get_unpossessed(self):
		_puppet = puppet;
		return;
	_disconnect_puppet(puppet)
	unpossessed.emit(puppet)


func toggle_possess(puppet: Puppet) -> void:
	if _puppet == puppet:
		unpossess(puppet);
	else: 
		possess(puppet)


func get_puppet() -> Puppet:
	return _puppet;


func has_puppet() -> bool:
	return is_instance_valid(_puppet);


func _connect_puppet(puppet: Puppet) -> void:
	puppet.tree_exited.connect(_on_puppet_tree_exited);


func _disconnect_puppet(puppet: Puppet) -> void:
	puppet.tree_exited.disconnect(_on_puppet_tree_exited);


func _on_puppet_tree_exited(puppet: Puppet) -> void:
	_puppet = null;
	if is_instance_valid(puppet):
		unpossess(puppet)
