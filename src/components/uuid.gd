class_name UUID
extends Node

## [U]niversally [U]nique [ID]entifier as a component.

enum ParentNameSync { 
	## Do nothing.
	NONE, 
	## Change the parent's name to the uuid.
	FORWARD, 
	## Change the uuid to the parent's name. [color=khaki]Not Implemented.[/color]
	BACKWARD, 
	## Attempt both of the previous options at once. [color=khaki]Not Implemented.[/color]
	BOTH, 
	}

## What the generator will "mask" the numbers to, sort of like remapping it, I think?
const BYTE_MASK: int = 0b11111111;
## A "nil" uuid, may be useful.
const NIL := "00000000-0000-0000-0000-000000000000";

static var _cache := {};

## The actual string identifier. Generated on init.
@export
var uuid := UUID.generate();
## Syncing the parent node name to and/or from the uuid for easier identification and use.
@export
var parent_name_sync := ParentNameSync.NONE;


func _enter_tree() -> void:
	_cache[uuid] = self;
	if parent_name_sync == ParentNameSync.FORWARD:
		get_parent().name = uuid;


func _exit_tree() -> void:
	if _cache.has(uuid):
		_cache.erase(uuid);


## Get a new uuid as a string.
static func generate() -> String:
	# actual code is from the uuid v4 addon and modified slightly, see here: 
	# https://github.com/binogure-studio/godot-uuid/blob/930d6d89bc3bc20cf48c0b4159834bc1cfa2d31f/addons/uuid/uuid.gd
	var bin := [
		# low
		randi() & BYTE_MASK, randi() & BYTE_MASK, randi() & BYTE_MASK, randi() & BYTE_MASK,
		# mid
		randi() & BYTE_MASK, randi() & BYTE_MASK, 
		# hi
		((randi() & BYTE_MASK) & 0x0f) | 0x40, randi() & BYTE_MASK,
		# clock
		((randi() & BYTE_MASK) & 0x3f) | 0x80, randi() & BYTE_MASK, 
		# clock
		randi() & BYTE_MASK, randi() & BYTE_MASK, randi() & BYTE_MASK, 
		randi() & BYTE_MASK, randi() & BYTE_MASK, randi() & BYTE_MASK,
	];
	return '%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x' % bin;


## Get the [UUID] component associated with a specific string uuid.
@warning_ignore("shadowed_variable")
static func get_node_by_uuid(uuid: String) -> UUID:
	if not has_uuid(uuid): return null;
	return _cache[uuid] as UUID;


## Returns whether the specified string uuid is in the cache or not.
@warning_ignore("shadowed_variable")
static func has_uuid(uuid: String) -> bool:
	return _cache.has(uuid);
